#! /usr/bin/env python

import csv
import argparse

# Read command line and set args
parser = argparse.ArgumentParser(prog='tf_gene_all2consist', description='Filters TF Gene relations by consistency rules between patterns')
parser.add_argument('-i', '--input_file',   nargs='?', help='TF_Gene_allProfiles.csv (CSV)')
parser.add_argument('-o', '--output_file', nargs='?', help='TF_Gene_ConsistRel.csv (hardcoded)')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.input_file or not args.output_file:
    parser.print_help()
    exit(1)

#/!\  #region [TF,gene]
not_contradictory_pos = [
        [
            #1
            [1,1],
            [2,1],
            [3,1],
            [4,1],
            [5,1],
            ],
        [
            #2
            [1,1],
            [2,1],
            [3,1],
            [4,2],
            ],
        [
            #3
            [1,1],
            [2,1],
            [3,2],
            [4,3],
            [5,5],
            ],
        [
            #4
            [1,1],
            [2,2],
            [3,3],
            [4,4],
            [5,5],
            ],
        [
            #5
            [1,1],
            [2,2],
            [3,3],
            [4,4],
#            [5,5],
            ],
        ]
not_contradictory_neg = [
        [
            #1
            [1,4],
            [2,4],
            [3,4],
            [4,4],
            [5,4],
            ],
        [
            #2
            [1,4],
            [2,4],
            [3,4],
            [4,3],
            ],
        [
            #3
            [1,4],
            [2,4],
            [3,3],
            [4,2],
            [5,5],
            ],
        [
            #4
            [1,4],
            [2,3],
            [3,2],
            [4,1],
            [5,5],
            ],
        [
            #5
            [1,4],
            [2,3],
            [3,2],
            [4,1],
#            [5,5],
            ],
        ]

mostly_not_contradictory_pos = [
        [
            #1
            [3,2],
            [4,2],
            [5,2],
            ],
        [
            #2
            [1,5],
            [2,2],
            [2,5],
            [3,2],
            [4,1],
            [4,3],
            [5,1],
            [5,2],
            [5,5],
            ],
        [
            #3
            [1,2],
            [2,2],
            [2,5],
            [3,1],
            [3,3],
            [3,5],
            [4,2],
            [4,4],
            [5,2],
            [5,3],
            ],
        [
            #4
            [1,2],
            [2,1],
            [2,3],
            [3,2],
            [3,4],
            [3,5],
            [4,3],
            [4,5],
            [5,3],
            [5,4],
            ],
        [
            #5
            [1,2],
            [2,1],
            [2,3],
            [3,2],
            [3,4],
            [3,5],
            [4,3],
            [4,5],
            [5,3],
            [5,4],
            ],
        ]
mostly_not_contradictory_neg = [
        [
            #1
            [3,3],
            [4,3],
            [4,3],
            ],
        [
            #2
            [2,3],
            [3,3],
            [3,5],
            [4,2],
            [4,4],
            [4,5],
            [5,3],
            [5,4],
            [5,5],
            ],
        [
            #3
            [1,3],
            [2,3],
            [3,5],
            [3,2],
            [3,4],
            [3,5],
            [4,1],
            [4,3],
            [5,2],
            [5,3],
            ],
        [
            #4
            [1,3],
            [1,5],
            [2,2],
            [2,4],
            [2,5],
            [3,1],
            [3,3],
            [4,2],
            [5,1],
            [5,2],
            ],
       [
            #5
            [1,3],
            [1,5],
            [2,2],
            [2,4],
            [2,5],
            [3,1],
            [3,3],
            [4,2],
            [5,1],
            [5,2],
            ],
        ]
  
id_reg=0

matrice = open(args.output_file, "a")
matrice.write("Gene\tGene_Pattern\tRegion\tRegion_Pattern\tTF\tTF_Pattern\tRegulation\n")
with open(args.input_file) as csvfile:
    dialect = csv.Sniffer().sniff(csvfile.read(1024))
    csvfile.seek(0)
    reader = csv.DictReader(csvfile, dialect=dialect)
    for row in reader:
        x=int(row['TF_Pattern'])
        z=int(row['Region_Pattern'])
        y=int(row['Gene_Pattern'])
        nb_cpl_ok_pos = 0
        nb_cpl_ok_neg = 0
        for mult in [1000, 100, 10, 1]:
            #getting each digit
            xx = int((x/mult) % 10)
            yy = int((y/mult) % 10)
            zz = int((z/mult) % 10)
            # we take zz-1 because python indices start at 0, not 1
            for pair in not_contradictory_pos[zz - 1]:
                if pair[0] == xx and pair[1] == yy:
                    nb_cpl_ok_pos = nb_cpl_ok_pos +2
                    break
            for pair in not_contradictory_neg[zz - 1]:
                if pair[0] == xx and pair[1] == yy:
                    nb_cpl_ok_neg = nb_cpl_ok_neg +2
                    break
            for pair in mostly_not_contradictory_pos[zz - 1]:
                if pair[0] == xx and pair[1] == yy:
                    nb_cpl_ok_pos = nb_cpl_ok_pos +1
                    break
            for pair in mostly_not_contradictory_neg[zz - 1]:
                if pair[0] == xx and pair[1] == yy:
                    nb_cpl_ok_neg = nb_cpl_ok_neg +1
                    break
        if nb_cpl_ok_pos > 6:
            matrice.write(row['Gene'] +"\t"+row['Gene_Pattern'] +"\t"+row['Region'] +"\t"+row['Region_Pattern'] +"\t"+row['TF'] +"\t"+row['TF_Pattern'] +"\t+\n")
        elif nb_cpl_ok_neg > 6:
            matrice.write(row['Gene'] +"\t"+row['Gene_Pattern'] +"\t"+row['Region'] +"\t"+row['Region_Pattern'] +"\t"+row['TF'] +"\t"+row['TF_Pattern'] +"\t-\n")
        elif x==5555 and y==5555 and z==5555:
            matrice.write(row['Gene'] +"\t"+row['Gene_Pattern'] +"\t"+row['Region'] +"\t"+row['Region_Pattern'] +"\t"+row['TF'] +"\t"+row['TF_Pattern'] +"\tND\n")

