#!/usr/bin/env python

import pandas as pd
import numpy as np
import argparse
from pprint import pprint

# Read command line and set args
parser = argparse.ArgumentParser(prog='gene_pattern_count', description='Count genes per pattern')
parser.add_argument('-g', '--gene_file',   nargs='?', help='Gene Patterns CSV file')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

if not args.gene_file or not args.output_file:
    parser.print_help()
    exit(1)

count = pd.read_csv(args.gene_file).groupby("Gene_Pattern").size().rename("Nb_genes_per_gene_pattern").to_csv(args.output_file)


