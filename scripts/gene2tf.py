#!/usr/bin/env python

import pandas as pd
import argparse

# Read command line and set args
parser = argparse.ArgumentParser(prog='gene2tf', description='Select TF patterns from Gene patterns given a list of TFs')
parser.add_argument('-g', '--gene_file',   nargs='?', help='Gene Patterns CSV file')
parser.add_argument('-t', '--tf_file',     nargs='?', help='List of TFs')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

if not args.gene_file or not args.tf_file or not args.output_file:
    parser.print_help()
    exit(1)

# Read input files
gene_df = pd.read_csv(args.gene_file) 
tf_df = pd.read_csv(args.tf_file, names=['TF']) 

# Select tf from gene patterns
tf_list = list(tf_df.TF.values)
res_df = gene_df[gene_df.Gene.isin(tf_list)]

# Rename 'Gene' column in 'Transcription_Factor'
columns = list(res_df.columns)
columns[0] = 'TF'
res_df.columns =columns
res_df = res_df.set_index('TF')
new_name = {'Gene_Pattern' : 'TF_Pattern'}
res_df = res_df.rename(columns=new_name)

# Write output file
res_df.to_csv(args.output_file)
