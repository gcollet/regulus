#!/usr/bin/env python

import pandas as pd
import argparse
import csv

# Read command line and set args
parser = argparse.ArgumentParser(prog='filter_dist', description='filter distances between regions and genes')
parser.add_argument('-g', '--gene_file',   nargs='?', help='Gene-Region distance file (CSV)')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.gene_file or not args.output_file:
    parser.print_help()
    exit(1)

full = pd.read_csv(args.gene_file)[['Region', 'Gene', 'Distance']]

# Filter and format output for ttl integration
# for each couple Region/Gene, keep the minimum distance
new_name = {'Region' : 'nextto@Region', 'Gene' : 'nextto@Gene'}
filter_min = full.groupby(['Region', 'Gene']).min().reset_index()
filter_min['id_Region_close'] = 'Region_close_' + filter_min.index.astype(str)
filter_min = filter_min.set_index('id_Region_close')
filter_min = filter_min.rename(columns=new_name)

filter_min.to_csv(args.output_file, index=True)