# Takes as argument a VOS sql file and executes it
# e.g. $sh virtuoso-run-script.sh enable-auto-indexing.sql
read -d '' part1 << EOF
-- Deleting previous entries of loader script 
delete from DB.DBA.load_list;
-- see http://www.openlinksw.com/dataspace/dav/wiki/Main/VirtBulkRDFLoader
select 'Loading data...';
-- ld_dir (<folder with data>, <pattern>, <default graph if no graph file specified>)
ld_dir (
EOF
read -d '' part2 << EOF
, '*.ttl', '');

rdf_loader_run();
EOF

COMMAND="${part1}'${1}'${part2}"

tmpfile=$(mktemp)
echo "$COMMAND" > $tmpfile
# <virtuoso isql path>  <isql port> <user> <port>
isql 1111 dba dba VERBOSE=OFF 'EXEC=status()' $tmpfile
