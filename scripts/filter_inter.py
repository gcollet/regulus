#!/usr/bin/env python

import pandas as pd
import argparse
import csv

# Read command line and set args
parser = argparse.ArgumentParser(prog='filter_inter', description='filter intersections between regions and TF')
parser.add_argument('-i', '--intersect_file',   nargs='?', help='TF-Region intersect file (CSV)')
parser.add_argument('-o', '--output_file', nargs='?', help='filename to output result')
args = parser.parse_args()

# If an argument is missing -> usage and exit
if not args.intersect_file or not args.output_file:
    parser.print_help()
    exit(1)

# Read tf-region intersect file
table_inter = pd.read_csv(args.intersect_file, header=None, delimiter='\t')
table_inter.columns = ['chr_Reg', 'start_Reg', 'end_Reg', 'Region', 'chr_TF', 'start_TF', 'end_TF', 'TF']
table_inter = table_inter.sort_values(['Region', 'TF'], ascending=[True, True])

table_inter = table_inter.drop_duplicates(['Region', 'TF'])
table_inter = table_inter[['Region', 'TF']]
table_inter['TF_inclusion_Region'] = 'TF_in_Region_' + table_inter.index.astype(str)
table_inter = table_inter.set_index('TF_inclusion_Region')
table_inter = table_inter.rename(columns={'Region' : 'has_binding_site_in@Region', 'TF' : 'binding_site_of_TF@TF'})

table_inter.to_csv(args.output_file, index=True)
